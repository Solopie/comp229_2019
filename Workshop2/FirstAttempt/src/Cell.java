package bin;

import java.awt.*;

class Cell {
    // Position of cell (Top-left corner)
    int x,y, size;

    Cell(int x, int y, int size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }

    void paint(Graphics g) {
        g.drawRect(x,y,size,size);
    }

    boolean mouseHover(double x, double y) {
        if(this.x <= x && this.x + size >= x && this.y <= y && this.y + size >= y) {
            return true;
        } else {
            return false;
        }

    }
}
