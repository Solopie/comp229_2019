package bin;

// Based from: http://greenteapress.com/thinkjava6/html/thinkjava6017.html

import java.awt.*;
import javax.swing.*;

class Drawing extends Canvas {
    // My personalised grid
    Grid myGrid = new Grid(20,20,35);

    public static void main(String[] args) {
        JFrame frame = new JFrame("My Drawing");
        Canvas canvas = new Drawing();
        canvas.setSize(720, 720);
        frame.add(canvas);
        frame.pack();
        frame.setVisible(true);
        
    }

    public void paint(Graphics g) {

        /* Task 3
        x is the horizontal axis and y is vertical axis
        for(int x = 10 ; x < getWidth(); x += 35) {
            for(int y = 10; y < getHeight(); y += 35) {
                g.drawRect(x,y,35,35);
            }
        }
        */

        // Task 4/5 Drawing the actual grid
        Cell[][] tempCells = myGrid.getCells();

        for(int i = 0 ; i < tempCells.length; i++) {
            for(int j = 0 ; j < tempCells[i].length; j++) {
                if(getMousePosition() != null && tempCells[i][j].mouseHover(getMousePosition().getX(), getMousePosition().getY())) {
                    // Color in cell
                    g.setColor(Color.BLACK);
                    g.fillRect(tempCells[i][j].x, tempCells[i][j].y, tempCells[i][j].size, tempCells[i][j].size);
                } else {
                    g.setColor(Color.WHITE);
                }
                g.setColor(Color.BLACK);
                g.drawRect(tempCells[i][j].x, tempCells[i][j].y, tempCells[i][j].size, tempCells[i][j].size);
            }
        }

        if(getMousePosition() != null) {
            System.out.println(getMousePosition().getX());
        }
        
        super.repaint();
    }

}
