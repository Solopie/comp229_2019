package bin;

class Grid {

    Cell[][] cells;


    // Parameters: Amount of rows, amount of columns, width of cell, height of cell
    Grid(int numRows, int numCols, int cellSize) {
        cells = new Cell[numRows][numCols];

        // Initialise cells
        // 10 off the top and left
        for(int i = 0 ; i < cells.length; i++) {
            for(int j = 0 ; j < cells[i].length; j++) {
                cells[i][j] = new Cell(i*35 + 10,j*35 + 10,cellSize);
            }
        }

    }

    Cell[][] getCells() {
        return cells;
    }
}
