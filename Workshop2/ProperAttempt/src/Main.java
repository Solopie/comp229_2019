package bin;

import java.awt.*;
import javax.swing.*;

public class Main extends JFrame {

    public Main() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Canvas canvas = new Canvas();
        this.setContentPane(canvas);
        this.pack();
        this.setVisible(true);

    }
    public static void main(String[] args) throws Exception {
        Main window = new Main();

        while(true) {
            window.repaint();
        }
    }

    public class Canvas extends JPanel {
        public Canvas() {
            setPreferredSize(new Dimension(720,720));
        }

        // This will override the Main's paint method
        @Override
        public void paint(Graphics g) {
            for(int i = 10; i < 710; i += 35) {
                for(int j = 10; j < 710; j += 35) {
                    g.drawRect(i,j,35,35);
                }
            }
        }


    }
}
