package bin;

import java.awt.*;

class Cell {
    int x,y,size;

    public Cell(int x, int y, int size) {
        this.x = x;
        this.y = y;
        this.size = size;
    }

    void paint(Graphics g, boolean isFill) {
        g.drawRect(x,y,size,size);
        if(isFill) {
            g.fillRect(x,y,size,size);
        }
    }

    // Task 5
    boolean mouseHover(double mouseX, double mouseY) {
        if(mouseX >= x && mouseX <= x + size && mouseY >= y && mouseY <= y + size) {
            return true;
        }
        return false;
    }
}
