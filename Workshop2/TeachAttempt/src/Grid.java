package bin;

import java.awt.*;

class Grid {
    Cell[][] cells;
    int sizeOfCell;

    public Grid(int length, int sizeOfCell) {
        cells = new Cell[length][length];

        for(int i = 0; i < cells.length; i++) {
            for(int j = 0; j < cells[i].length; j++) {
                cells[i][j] = new Cell(i*sizeOfCell + 10, j*sizeOfCell + 10, sizeOfCell);
            }
        }
    }

    void paint(Graphics g, boolean onScreen, double mouseX, double mouseY) {
        for(int i = 0; i < cells.length; i++) {
            for(int j = 0; j < cells[i].length; j++) {
                if(onScreen) {
                    if(cells[i][j].mouseHover(mouseX, mouseY)) {
                        cells[i][j].paint(g, true);
                    } else {
                        cells[i][j].paint(g, false);
                    }
                } else {
                    cells[i][j].paint(g, false);
                }
            }
        }
    }
}
