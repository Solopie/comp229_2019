package bin;

import java.awt.*;
import javax.swing.*;

class Main extends JFrame{
    public class Canvas extends JPanel{
        public Canvas() {
            setPreferredSize(new Dimension(720,720));
        }

        @Override
        public void paint(Graphics g) {
            Grid myGrid = new Grid(20,35);

            // Task 5
            if(getMousePosition() != null) {
                myGrid.paint(g, true, getMousePosition().getX(), getMousePosition().getY());
            } else {
               myGrid.paint(g, false, 0, 0);
            }
        }
    }

    public Main() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Canvas canvas = new Canvas();
        this.setContentPane(canvas);
        this.pack();
        this.setVisible(true);
    }

    public static void main(String[] args) throws Exception {
        Main window = new Main();

        // Task 5
        while(true) {
            window.repaint();
        }
    }
}
